const express = require("express");
const bodyParser = require("body-parser");

const app = express();

const PORT = process.env.PORT || 5000;

app.use(bodyParser.json());

const fetch = require("node-fetch")
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');




// Request Handler
app.post('/signup', async (req, res) => {
  const HASURA_OPERATION = `
    mutation ($interface: Int, $email: String, $password: String) {
      insert_Users_one(object:{
        interface:$interface,
        email:$email,
        password:$password
      }) {
        id
      }
    }
  `;

  // execute the parent operation in Hasura
  const execute = async (variables) => {
    const fetchResponse = await fetch(
      `http://${process.env.HASURA_URL}:8080/v1/graphql`,
      {
        method: 'POST',
        headers: { 'x-hasura-admin-secret': process.env.HASURA_GRAPHQL_ADMIN_SECRET },
        body: JSON.stringify({
          query: HASURA_OPERATION,
          variables
        })
      }
    );
    const data = await fetchResponse.json();
    console.log('DEBUG: ', data);
    return data;
  };

  // get request input
  const { interface, email, password } = req.body.input;

  // run some business logic
  let hashedPassword = await bcrypt.hash(password, 10);

  // execute the Hasura operation
  const { data, errors } = await execute({ interface, email, password: hashedPassword });

  // if Hasura operation errors, then throw error
  if (errors) {
    return res.status(400).json(errors[0])
  }

  const tokenContents = {
    sub: data.insert_Users_one.id.toString(),
    interface: interface.toString(),
    email: email,
    iat: Date.now() / 1000,
    iss: 'https://myapp.com/',
    "https://hasura.io/jwt/claims": {
      "x-hasura-allowed-roles": ["user"],
      "x-hasura-user-id": data.insert_Users_one.id.toString(),
      "x-hasura-default-role": "user",
      "x-hasura-role": "user"
    },
    exp: Math.floor(Date.now() / 1000) + (24 * 60 * 60)
  }

  const token = jwt.sign(tokenContents, process.env.ENCRYPTION_KEY);

  // success
  return res.json({
    ...data.insert_Users_one,
    token: token
  })

});


// Request Handler
app.post('/login', async (req, res) => {

  // get request input
  const { interface, email, password } = req.body.input;

  const HASURA_OPERATION = `
  query ($interface: Int!, $email:String!){
    Users(where: {interface: {_eq: $interface}, email: {_eq: $email}}){
      id
      email
      password
    }
  }
  `;

  // execute the parent operation in Hasura
  const execute = async (variables) => {
    delete variables.password
    const fetchResponse = await fetch(
      `http://${process.env.HASURA_URL}:8080/v1/graphql`,
      {
        method: 'POST',
        headers: { 'x-hasura-admin-secret': process.env.HASURA_GRAPHQL_ADMIN_SECRET },
        body: JSON.stringify({
          query: HASURA_OPERATION,
          variables
          
        })
      }
    );
    const data = await fetchResponse.json();
    console.log('DEBUG: ', data);
    return data;
  };

  
  // execute the Hasura operation
  const { data, errors } = await execute({ interface, email, password });
  
  // if Hasura operation errors, then throw error
  if (errors) {
    return res.status(400).json(errors[0])
  }
  // run some business logic
  const verified = bcrypt.compareSync(password, data.Users[0].password);

  if (!verified) {
    return res.status(401).json({"message": "Invalid Password"})
  }

  const tokenContents = {
    sub: data.Users[0].id.toString(),
    interface: interface.toString(),
    email: email,
    iat: Date.now() / 1000,
    iss: 'https://myapp.com/',
    "https://hasura.io/jwt/claims": {
      "x-hasura-allowed-roles": ["user"],
      "x-hasura-user-id": data.Users[0].id.toString(),
      "x-hasura-default-role": "user",
      "x-hasura-role": "user"
    },
    exp: Math.floor(Date.now() / 1000) + (24 * 60 * 60)
  }

  const token = jwt.sign(tokenContents, process.env.ENCRYPTION_KEY);

  // success
  return res.json({
    token
  })

});

app.listen(PORT);
